package cn.doitedu.ml.demo

import org.apache.spark.ml.linalg
import org.apache.spark.ml.linalg.Vectors

/**
 * @date: 2020/2/17
 * @site: www.doitedu.cn
 * @author: hunter.d 涛哥
 * @qq: 657270652
 * @description: SPARK MLLIB包中的 Vector编程接口api使用示例
 */
object VectorDemo {

  def main(args: Array[String]): Unit = {

    // 需求： 有一行字符串表达的特征向量 "30,172,58,2000" 需要封装成mllib中的Vector
    val p1: String = "30,172,58,2000"  // 这个向量本身来说，是一个密集型的
    val featureArray: Array[Double] = p1.split(",").map(_.toDouble)

    // 1. 将特征值数组，封装成Dense特征向量
    val vector: linalg.Vector = Vectors.dense(featureArray)

    // 得出向量的长度（特征的个数）
    val size = vector.size

    // 得出向量中封装的特征值数组
    val array = vector.toArray

    // 取向量中指定位置的特征值
    val feature2 = vector(2)

    // 将密集模型向量转成稀疏模型向量
    val sparseVector = vector.toSparse

    // 找到向量中最大特征值所在的脚标
    val maxFeatureIndex = vector.argmax



    // 2. 将特征值数组，封装成Sparse特征向量
    // val p2: String = "30,0,58,0,0,0"  // 这个向量本身来说，是一个密集型的

    val vector2: linalg.Vector = Vectors.sparse(6,Array(0,2),Array(30,58))

    // 得出向量的长度（特征的个数）
    val size2 = vector2.size

    // 找到向量中最大特征值所在的脚标
    val maxFeatureIndex2 = vector2.argmax

    // 从向量中取指定脚标上的特征值
    val feature3 = vector2.apply(3)

    // 得到向量的特征值数组
    val array2 = vector2.toArray
    println(array2.mkString(","))

    // 将稀疏模型对象转成密集模型对象
    val denseVector = vector2.toDense
    println(denseVector)


  }

}
