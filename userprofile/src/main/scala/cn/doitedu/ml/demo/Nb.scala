package cn.doitedu.ml.demo

import cn.doitedu.commons.util.SparkUtil
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.Dataset


case class A(id: Int, name: String)
case class B(id: Int, addr: String)
object Nb {

  def main(args: Array[String]): Unit = {
    Logger.getLogger("org.apache.spark").setLevel(Level.WARN)
    val spark = SparkUtil.getSparkSession("")
    import spark.implicits._

    val ds = spark.createDataset(Seq("1,zs,1,beijing"))
    val res: Dataset[(A, B)] = ds.map(s => {
      val arr = s.split(",")
      (A(arr(0).toInt, arr(1)), B(arr(2).toInt, arr(3)))
    })
    res.printSchema()
    res.show()

    spark.close()


  }

}
