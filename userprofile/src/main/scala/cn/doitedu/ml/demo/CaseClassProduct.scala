package cn.doitedu.ml.demo

case class Student(id:Int,name:String)

object CaseClassProduct {

  def main(args: Array[String]): Unit = {

    val stu = new Student(1,"azkaban")

    // 以下这些方法，都是product特质上的
    println(stu.productElement(0))
    println(stu.productArity)
    println(stu.productPrefix)
    val iter = stu.productIterator
    iter.foreach(println)





  }

}
