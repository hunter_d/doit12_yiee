package cn.doitedu.ml.demo

import scala.beans.BeanProperty

/**
  * scala中的普通类
  */
class ScalaPerson(
     @BeanProperty
     var id:Int,
     @BeanProperty
     var name:String,
     @BeanProperty
     var age:Int
     )


