package cn.doitedu.ml.demo

import java.util

import com.hankcs.hanlp.HanLP
import com.hankcs.hanlp.dictionary.py.Pinyin
import com.hankcs.hanlp.seg.common.Term

/**
 * @date: 2020/2/23
 * @site: www.doitedu.cn
 * @author: hunter.d 涛哥
 * @qq: 657270652
 * @description: Hanlp自然语言处理工具包 api调用示例
 */
object HanLpDemo {
  def main(args: Array[String]): Unit = {

    import scala.collection.JavaConversions._

    val text = "多易教育十二期的同学都非常优秀"

    // 中文转拼音
    val pinyins: util.List[Pinyin] = HanLP.convertToPinyinList(text)
    for(i <- 0 until pinyins.size()){
      println(pinyins.get(i).getPinyinWithoutTone)
    }


    // 提取文档关键词
    val cmt="使用了一个月再来评价，给后买的人有个心理预期。前置指纹识别，6G运行内存，差不多的硬件比MATE10便宜近千元，冲着这几点入手了，但使用中跟预期有点落差。打开APP速度很快，这点很好。AI，宣传大于实用，能在拍照时自动识别场景，而且不是全部都能，没看出有什么过人之处。最大的败笔是屏幕，黑边控制得稍好，边框没有想象中窄，第一次买所谓的全面屏手机，完全没有惊艳感觉。斜纹虽没有网上传闻的那么严重，但细心看还是有，屏幕色彩饱和度偏低，偏白，感觉灰白灰白的，跟在用的MATE8屏幕差太远了，连很多千元机都不如！"
    val keywords: util.List[String] = HanLP.extractKeyword(cmt,8)
    keywords.foreach(println)


    // 提取摘要
    val doc = "2月21日，习近平总书记主持召开中共中央政治局会议。会议要求各级党委和政府要贯彻党中央关于疫情防控各项决策部署，毫不放松抓好疫情防控工作，及时完善防控策略和措施，不断巩固成果、扩大战果，全面打赢疫情防控人民战争、总体战、阻击战。\n\n新冠肺炎疫情发生后，党中央高度重视，习近平总书记时刻关注疫情形势，把疫情防控作为头等大事来抓，亲自指挥、亲自部署，提出坚定信心、同舟共济、科学防治、精准施策的总要求。党中央及时制定疫情防控方针政策，确保疫情防控有力有序推进，坚决遏制疫情扩散蔓延。\n\n\n\n经过艰苦努力，在党中央集中统一领导下，各级党委、政府艰苦工作、恪守职责，全国人民同舟共济、顽强奋战，疫情蔓延势头得到初步遏制，防控工作取得阶段性成效，全国新增确诊病例数和疑似病例数总体呈下降趋势，治愈出院人数较快增长，尤其是湖北以外省份新增病例大幅减少。这充分说明党中央采取的一系列防控方针和政策十分正确有效，大大增强了我们战胜“疫”魔的信心决心。\n\n从疫情防控实践来看，湖北依然是我们能否彻底打赢这场疫情防控阻击战的决定要素。近一段时间，全国各地紧急支援，疫情蔓延、物资短缺的紧张局面得到基本控制。但我们要清醒认识到，全国疫情发展拐点尚未到来，湖北省和武汉市防控形势依然严峻复杂，任务依然繁重，不容丝毫麻痹松懈、马虎大意。\n\n\n\n要突出重点，打好湖北保卫战、武汉保卫战。我们要紧紧围绕提高收治率和治愈率、降低感染率和病亡率的目标，牢牢抓住救治、阻隔两大关键环节。要把重点放在彻底切断传染源上，把排查工作做细，对居家患者进行地毯式、拉网式排查，摸清底数，实现“清底”；把治疗工作做实，加快方舱医院改造建设、床位扩容，对现有病患应收尽收，防止交叉感染；把防控工作做深，加大医务人员和医用物资支持力度，加强力量薄弱地区防控，防止新的感染扩散。\n\n\n\n要因地制宜，因事制宜，不断完善差异化防控策略，根据疫情轻重程度有序安排本区域现阶段重点工作。在非疫情严重地区，广大领导干部要勇于担当，实事求是，一手抓疫情防控阻击战不放松，巩固防治成果、避免反弹，牢牢把握防控工作的主动权；一手抓经济社会发展不动摇，组织复工复产有序开展，充分利用财政、金融等政策工具加强对经济运行的调度，扩大消费需求，释放新的潜力，加大民生托底保障力度，坚定不移打好三大攻坚战。\n\n我们坚信，在以习近平同志为核心的党中央坚强领导下，全党全国坚定信心、同舟共济、科学防治、精准施策，我们一定能全面打赢疫情防控的人民战争，共同迎接春天的到来！\n"
    val summary: util.List[String] = HanLP.extractSummary(doc,4)
    summary.foreach(s=>print(s+","))

    println("----------")

    // 中文分词
    val comment = "外形是挺好看的，运行速度也快，但是这触控也太不灵敏了吧，经常需要摁半天才有反应，以及手机也容易发烫总之没想象中的好，也就一般吧"
    val terms: util.List[Term] = HanLP.segment(comment)
    //terms.foreach(term=>println(term))

    terms.foreach(term=>{
      val nature = term.nature
      val word = term.word
      println(word,nature)
    })


    val filtered = terms.filter(term=>{
      !term.nature.startsWith("d") &&
      !term.nature.startsWith("p") &&
      !term.nature.startsWith("w")
    })


    println("*********************")
    filtered.foreach(println)



  }

}
