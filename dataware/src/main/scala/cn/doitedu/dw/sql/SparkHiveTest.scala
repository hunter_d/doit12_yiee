package cn.doitedu.dw.sql

import org.apache.spark.sql.SparkSession

object SparkHiveTest {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName(this.getClass.getSimpleName)
      .master("local")
      .enableHiveSupport()
      .getOrCreate()

    import spark.implicits._

    val df = spark.sql(
      """
        |
        |select * from doit12.demo_dws_apl_uca_rng
        |
      """.stripMargin)

    df.show(50,false)


    spark.close()
  }

}
