import java.util.Calendar

import org.apache.commons.lang.time.DateUtils

object DateUtilDemo {

  def main(args: Array[String]): Unit = {

    val date1 = DateUtils.parseDate("2020-05-22",Array("yyyy-MM-dd"))
    val date2 = DateUtils.parseDate("2020-05-22",Array("yyyy-MM-dd"))
    val day1 = DateUtils.getFragmentInDays(date1,Calendar.YEAR)
    val day2 = DateUtils.getFragmentInDays(date2,Calendar.YEAR)

    println(day1,day2,day2-day1)



  }

}
