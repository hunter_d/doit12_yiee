import java.util.Calendar

import org.apache.commons.lang.time.{DateFormatUtils, DateUtils}

object HelloScala {

  def main(args: Array[String]): Unit = {

    val map = Map("a"->"zhangsan","b"->"lisi")


    // Some("zhangsan")
    val maybeString1: Option[String] = map.get("a")
    println(maybeString1.get)


    // None
    val maybeString2: Option[String] = map.get("c")
    //println(maybeString2.get)


    val dt = DateUtils.parseDate("2020-01-19",Array("yyyy-MM-dd"))
    val date = DateUtils.addDays(dt,60)
    println(DateFormatUtils.format(dt,"yyyy-MM-dd"))
    println(DateFormatUtils.format(date,"yyyy-MM-dd"))

    val day1 = DateUtils.getFragmentInDays(dt,Calendar.YEAR)
    val day2 = DateUtils.getFragmentInDays(date,Calendar.YEAR)

    println(day1)
    println(day2)

  }

}
