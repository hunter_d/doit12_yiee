import org.apache.commons.codec.digest.DigestUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession

object ReadParquet {

  def main(args: Array[String]): Unit = {

    Logger.getLogger("org.apache.spark").setLevel(Level.WARN)

    val spark = SparkSession.builder()
      .appName(this.getClass.getSimpleName)
      .master("local")
      .getOrCreate()


    val df = spark.read.parquet("data/idmp/2020-01-12")

    df.show(50,false)


    spark.close()

  }

}
