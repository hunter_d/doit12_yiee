//import cn.doitedu.commons.util.SparkUtil
//import org.apache.spark.sql.Row
//
//case class Person(id:Int,name:String,fanfou:Boolean)
//
//object CaseMatch {
//
//  def main(args: Array[String]): Unit = {
//
//
//    val p = Person(1,"薛海宽",false)
//
//
//    p match {
//      case x:Person => println(x.name,x.id,x.fanfou)
//    }
//
//
//    p match {
//      case Person(x:Int,y:String,z:Boolean) => print(x,y,z)
//    }
//
//
//    p match {
//      case z:Person =>  println(z.id)
//      case y:AnyRef =>  println(y)
//    }
//
//
//    val spark = SparkUtil.getSparkSession()
//    import spark.implicits._
//    val df = spark.createDataset(Seq(1,2,3,4,5,6)).toDF("shuzi")
//    df.map(row=> row match {
//      case Row(x:Int) =>  (x,1)
//    })
//
//    df.map({
//      case Row(x:Int,y:String,z:Double) =>  {
//        x
//        y
//        z
//      }
//    })
//
//
//    df.map(row=>{
//      val shuzi = row.getAs[Int]("shuzi")
//    })
//
//
//
//  }
//
//}
